import os
import platform
import logging
import sys
import tkinter as tk
from tkinter import filedialog


log_file = os.path.join(os.getenv('HOME'), 'wc.log')

print('Tworzenie logu w' + log_file)

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s : %(levelname)s : %(message)s',
    filename=log_file,
    filemode='w',
)


def name_info(f):
    def pack(*args, **kwds):
            logging.info("Wykonywanie funkcji " + f.__name__)
            return f(*args, **kwds)
    return pack


class WC():
    logging.debug("Start")

    def __init__(self):
        self.lines_number = 0
        self.words_number = 0
        self.bytes_number = 0

    @name_info
    def dict_fill(self, words, word_dict):
        logging.info("Wypełnianie słownika- słowo : liczba wystąpień")

        for word in words:



            if word in word_dict:
                word_dict[word] += 1
            else:
                word_dict[word] = 1

        return(word_dict)

    @name_info
    def list_make(self, word_dict):

        logging.info("Tworzenie  listy tupli (słowo: liczba wystąpień)")
        temp_list = []

        for key, value in word_dict.items():
            temp_list.append((value, key))

        temp_list.sort()
        return temp_list

    @name_info
    def output_format(self, input_list):

        input_list.sort(reverse=True)
        path = os.path.realpath(output_file)

        if os.path.isfile(path):
            os.unlink(output_file)
        try:


            openfile = open(output_file, 'a')
            openfile.write("%-16s %16d\n" % ('lines', self.lines_number))
            openfile.write("%-16s %16d\n" % ('all words', self.words_number))
            openfile.write("%-16s %16d\n" % ('bytes', self.bytes_number))
            openfile.write("********************************************\n")
            openfile.close()
            logging.info("Formatowanie outputu i zapisywanie do pliku")

            for count, word in input_list:
                openfile = open(output_file, 'a')
                openfile.write("%-16s %16d\n" % (word, count))
                openfile.close()
            return output_file

        except IOError:
            logging.warning('Nie udało sie utworzyc pliku')
            print('Nie udało sie utworzyc pliku')

    @name_info
    def word_count(self, input_file):
        try:
            logging.info("Otwieranie pliku")
            count_data = open(input_file, "r")
            self.bytes_number = os.path.getsize(input_file)
            word_dict = {}
            split_mark = ''
            for line in count_data:
                WC.dict_fill(self,  line.split(), word_dict)
                self.lines_number = self.lines_number + 1


            for counting_words in  word_dict.values():
                self.words_number = self.words_number + counting_words

            count_data.close()
            final_list = WC.list_make(self, word_dict)
            return WC.output_format(self, final_list)


        except IOError:
            logging.warning('Nie znaleziono pliku')
            print('Nie znaleziono pliku')

if len(sys.argv) > 2:
    input_file_name = sys.argv[1]
    output_file = sys.argv[2]
else:
    root = tk.Tk()
    root.withdraw()
    input_file_name = filedialog.askopenfilename()

    root = tk.Tk()
    root.withdraw()
    output_file = filedialog.asksaveasfilename()

if __name__ == "__main__":
    wc_object = WC()
    print (wc_object.word_count(input_file_name))


