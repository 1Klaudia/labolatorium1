import xml.dom.minidom
import os
import platform
import logging


log_file = os.path.join(os.getenv('HOME'), 'test_pars.log')

print('Tworzenie logu w' + log_file)

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s : %(levelname)s : %(message)s',
    filename=log_file,
    filemode='w',
)


class BookParser:

    logging.debug("Start")

    def parser(self, book_file):

        logging.info("Wykonywanie funkcji parser")
        try:
            DOMTree = xml.dom.minidom.parse(book_file)
            collection = DOMTree.documentElement

            books = collection.getElementsByTagName("book")

            tuple_list = []

            for book in books:

                if book.hasAttribute("id"):
                    id = book.getAttribute("id")

                author = book.getElementsByTagName('author')[0]
                tuple_author = author.childNodes[0].data

                title = book.getElementsByTagName('title')[0]
                tuple_title = title.childNodes[0].data

                genre = book.getElementsByTagName('genre')[0]
                tuple_genre = genre.childNodes[0].data

                price = book.getElementsByTagName('price')[0]
                tuple_price = price.childNodes[0].data

                publish_date = book.getElementsByTagName('publish_date')[0]
                tuple_publish_date = publish_date.childNodes[0].data

                description = book.getElementsByTagName('description')[0]
                tuple_description = description.childNodes[0].data

                tuple_list = (("Book id: " + id, "Author: " + tuple_author, "Title: " + tuple_title, "Genre: " + tuple_genre, "Price: " + tuple_price, "Publish date: " + tuple_publish_date, "Description: " + tuple_description))
                yield tuple_list


        except IOError:
            logging.warning("Nie mozna otworzyc pliku")

if __name__ == '__main__':
    book = BookParser()
    for x in book.parser('Books.xml'):
        print(x)
