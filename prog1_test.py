from unittest import TestCase
import unittest
from unittest.mock import patch, mock_open
import unittest.mock
import prog1


class TestProg1(TestCase):

    def test_if_function_make_list_of_tuple_from_ditionary_items(self):
        wc = prog1.WC()
        dict = {'jest': 1, 'test': 2, 'drugi': 1}
        expected_list = [(1, 'drugi'), (1, 'jest'), (2, 'test')]

        self.assertEqual(wc.list_make(dict), expected_list)


    def test_if_function_counts_words_from_the_list_and_add_to_dict(self):
        wc = prog1.WC()
        dict = {}
        list_of_words = ['ono', 'bum', 'bum']
        expected_dict = {'ono': 1, 'bum': 2}
        self.assertEqual(wc.dict_fill(list_of_words, dict), expected_dict)

    def test_if_function_format_list_and_save_it_in_file(self):
        wc = prog1.WC()
        input_list = [(1, 'ono'), (2, 'bum')]
        wc.output_format(input_list)

        file_test = open(wc.output_format(input_list))

        data = file_test.read()
        expected_data = ("%-16s %16d\n%-16s %16d\n%-16s %16d\n" % ('lines', 0, 'all words', 0, 'bytes',0) +"********************************************\n"+"%-16s %16d\n%-16s %16d\n" % ('bum', 2, 'ono', 1))
        file_test.close()
        self.assertEqual(data, expected_data)


    def test_if_function_word_counter_works_if_file_exist(self):
        wc = prog1.WC()
        test_file = 'test_file'
        path_to_file = wc.word_count(test_file)
        returned_file = open(path_to_file)
        data = returned_file.read()
        expected_data = ("%-16s %16d\n%-16s %16d\n%-16s %16d\n" % ('lines', 1, 'all words', 3, 'bytes',11) +"********************************************\n"+"%-16s %16d\n%-16s %16d\n" % ('to', 2, 'jest', 1))
        self.assertEqual(data, expected_data)


    def test_if_function_word_counter_except_IOError(self):
        wc = prog1.WC()
        test_file = 'nie_ma_mnie'
        self.assertRaises(IOError,wc.word_count(test_file))


if __name__ == '__main__':
    unittest.main()
