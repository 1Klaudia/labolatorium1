from unittest import TestCase
import unittest
from pars import BookParser


class BooksXMLParserTestClass(TestCase):

    def test__if_parser_parse_test_file_correctly(self):

        test_file = 'test.xml'
        book = BookParser()
        book_list = []
        book_data = (book.parser(test_file))
        for i in book_data:
            book_list.append(i)
        self.assertEqual(len(book_list),2)
        expected_item1 = ('Book id: bk101', 'Author: Klaudia Biela', "Title: XML jakaś książka", 'Genre: Komputer i ogólne szaleństwo', 'Price: 50.99', 'Publish date: 3000-10-01', 'Description: Opis który nie ma znaczenia.')

        expected_item2 = ('Book id: bk102', 'Author: Ja i Ty', "Title: Świat i takie tam ", 'Genre: Fantasy', 'Price: 333', 'Publish date: 4500-12-16', 'Description: Kolejny opis bez znaczenia.')



        for i in range(len(expected_item1)):
            self.assertEqual(book_list[0][i], expected_item1[i])

        for i in range(len(expected_item2)):
            self.assertEqual(book_list[1][i], expected_item2[i])

    def test_if_parser_raise_IOException_when_file_doesnot_exist(self):
        book = BookParser()
        test_file = 'idontexist.xml'
        self.assertRaises(IOError,book_data = (book.parser(test_file)))




if __name__ == '__main__':
    unittest.main()

